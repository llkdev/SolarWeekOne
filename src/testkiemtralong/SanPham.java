/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testkiemtralong;

/**
 *
 * @author LEOLAKI
 */
public class SanPham {
    private String tenSanPham;
    private String giaTien;

    public SanPham(String tenSanPham, String giaTien) {
        this.tenSanPham = tenSanPham;
        this.giaTien = giaTien;
    }

    public SanPham() {
    }

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public String getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(String giaTien) {
        this.giaTien = giaTien;
    }
    
    
}

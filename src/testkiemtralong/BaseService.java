/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testkiemtralong;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author LEOLAKI
 */
public class BaseService {
    public void themSinhVien(Document document,Element cuahang,KhachHang khachHang)
    {
        
        Element khachang=document.createElement("khachang");
        
        Element makhachhang=document.createElement("makhachhang");
        makhachhang.setTextContent(khachHang.getMaKhachHang());
        
        Element hoten=document.createElement("hoten");
        hoten.setTextContent(khachHang.getHoTen());
        
        Element sanpham=document.createElement("sanpham");
        
        
        Element tensanpham=document.createElement("tensanpham");
        tensanpham.setTextContent(khachHang.getSanPham().getTenSanPham());
        
        Element giatien=document.createElement("giatien");
        giatien.setTextContent(khachHang.getSanPham().getGiaTien());
        
        Element diachi=document.createElement("diachi");
        diachi.setTextContent(khachHang.getDiaChi());
        
        sanpham.appendChild(tensanpham);
        sanpham.appendChild(giatien);
        
        khachang.appendChild(makhachhang);
        khachang.appendChild(hoten);
        khachang.appendChild(sanpham);
        khachang.appendChild(diachi);
        
        cuahang.appendChild(khachang);
        
        
    }
    
    public void ghiFile(String filename,Document document)
    {
        try {
            TransformerFactory transformerFactory=TransformerFactory.newInstance();
            Transformer transformer=transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount","4");
            DOMSource nguon=new DOMSource(document);
            StreamResult dich=new StreamResult(filename);
            transformer.transform(nguon, dich);
        } catch (TransformerException ex) {
            Logger.getLogger(BaseService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

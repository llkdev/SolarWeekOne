/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testkiemtralong;

/**
 *
 * @author LEOLAKI
 */
public class KhachHang {
    private String maKhachHang;
    private String hoTen;
    private SanPham sanPham;
    private String diaChi;

    public KhachHang(String maKhachHang, String hoTen, SanPham sanPham, String diaChi) {
        this.maKhachHang = maKhachHang;
        this.hoTen = hoTen;
        this.sanPham = sanPham;
        this.diaChi = diaChi;
    }

    public KhachHang() {
    }

    public String getMaKhachHang() {
        return maKhachHang;
    }

    public void setMaKhachHang(String maKhachHang) {
        this.maKhachHang = maKhachHang;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public SanPham getSanPham() {
        return sanPham;
    }

    public void setSanPham(SanPham sanPham) {
        this.sanPham = sanPham;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testkiemtralong;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author LEOLAKI
 */
public class TaoFile {
    static BaseService baseService = new BaseService();
    static String filename = "src/testkiemtralong/cuahang.xml";
    
    public static void main(String[] args) {
        taoKhachhang();
    }
    public static void taoKhachhang() {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            
            Element cuahang = document.createElement("cuahang");
            
            KhachHang khachHang=new KhachHang("001", "Ho Ba Hung", new SanPham("I Phone", "100000"), "Nghe An");
            baseService.themSinhVien(document, cuahang, khachHang);
            
            document.appendChild(cuahang);
            
            baseService.ghiFile(filename, document);
            
            System.out.println("Da Ghi Xong!");
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(TaoFile.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}
